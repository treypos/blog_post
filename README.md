# RailsBlog-rubyschool
Blog_Post. Ruby on Rails.

Учебный проект на Ruby для разбора как работает Ruby on Rails.

## Start project

1. Install gems:

```bash
bundle install
```

2. Migrate:

```bash
rake db:migrate
```

3. Run RoR application:

```bash
rails s
```

4. Open in your browser: http://localhost:3000/