require "rails_helper"

describe Article do 
  describe "validations" do 
    it { should validate_presence_of :title }
    it { should validate_presence_of :text }
  end

  describe do "associations"
    it { should have_many :comments }
  end

  describe "#subject" do
    it "returns the article title" do
      # создаем объект article 
      article = create(:article, title: 'Foo Bar')
      # assert, проверка 
      expect(article.subject).to  eq 'Foo Bar'
    end
  end

  describe "#last_comment" do 
    it "returns the last comment" do 
      # создаем статью с комментариями (factories/articles)
      article = create(:article_with_comments)
      
      # проверка 
      expect(article.last_comment.body).to eq "comment body 3"
    end
  end
end
