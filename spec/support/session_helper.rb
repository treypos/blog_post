def sign_up 
  visit new_user_registration_path

  fill_in :user_email, :with => 'user@example.com'
  fill_in :user_username, :with => 'example123'
  fill_in :user_password, :with => 'example1212'
  fill_in :user_password_confirmation, :with => 'example1212'
  
  click_button 'Sign up'
end